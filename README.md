# Installation
```bash
kubectl create ns superset

helm upgrade --install superset --namespace superset ./charts/superset
```
